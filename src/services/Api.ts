import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'

export const instance = axios.create({
  baseURL: 'https://books.ioasys.com.br/api/v1',
  headers: {
    'Content-type': 'application/json'
  }
})

instance.interceptors.request.use(async config => {
  try {
    if (!config.headers.Authorization) {
      const token = await AsyncStorage.getItem('@books-app:user-token')

      if (token && token != null) {
        config.headers.Authorization = `Bearer ${token}`
      }
    }
    return config
  } catch (error) {
    return config
  }
})

export default instance