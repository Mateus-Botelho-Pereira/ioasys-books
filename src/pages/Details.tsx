import React, { useEffect, useState } from 'react'
import { StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, Image, View, Text} from 'react-native'
import { useNavigation } from '@react-navigation/core'

import api from '../services/Api'

import Back from '../../assets/Back.png'
import TestBook from '../../assets/TestBook.png'

import colors from '../styles/colors'
import fonts from '../styles/fonts'
import { Book } from '../model/book'

export function Details({ route }:any) {
    const { id } = route.params;
    const navigation = useNavigation()
    const [book, setBook] = useState<Book>()

    useEffect(() => {
        getBookRequest()
    }, [])

    const getBookRequest = async () => {
        const { data: book } = await api.get(`/books/${id}`)
        setBook(book)
    }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView>
                {book && <View style={styles.content}>
                    <TouchableOpacity style={styles.buttonBack} onPress={() => navigation.goBack()}> 
                        <Image source={Back}/>
                    </TouchableOpacity>
                    <Image style={styles.bookImage} source={{uri: book.imageUrl}}/>
                    <View style={styles.textContainer}>
                        <Text style={styles.bookTitle}>{book.title}</Text>
                        <Text style={styles.bookAuthor}>{book.authors.join(", ")}</Text>
                        <Text style={styles.bookInformation}>INFORMAÇÕES</Text>
                        <View style={styles.bookInformationList}>
                            <View style={styles.bookPropertiesGroup}>
                                <Text style={styles.bookProperties}>Páginas</Text>
                                <Text style={styles.bookPropertiesApi}>{book.pageCount} páginas</Text>
                            </View>  
                            <View style={styles.bookPropertiesGroup}>
                                <Text style={styles.bookProperties}>Editora</Text>
                                <Text style={styles.bookPropertiesApi}>Editora {book.publisher}</Text>
                            </View>
                            <View style={styles.bookPropertiesGroup}>
                                <Text style={styles.bookProperties}>Publicação</Text>
                                <Text style={styles.bookPropertiesApi}>{book.published}</Text>
                            </View>
                            <View style={styles.bookPropertiesGroup}>
                                <Text style={styles.bookProperties}>Idioma</Text>
                                <Text style={styles.bookPropertiesApi}>{book.language}</Text>
                            </View>
                            <View style={styles.bookPropertiesGroup}>
                                <Text style={styles.bookProperties}>Título Original</Text>
                                <Text style={styles.bookPropertiesApi}>{book.title}</Text>
                            </View>
                            <View style={styles.bookPropertiesGroup}>
                                <Text style={styles.bookProperties}>ISBN-10</Text>
                                <Text style={styles.bookPropertiesApi}>{book.isbn10}</Text>
                            </View>
                            <View style={styles.bookPropertiesGroup}>
                                <Text style={styles.bookProperties}>ISBN-13</Text>
                                <Text style={styles.bookPropertiesApi}>{book.isbn13}</Text>
                            </View>
                            <View style={styles.bookPropertiesGroup}>
                                <Text style={styles.bookProperties}>Categoria</Text>
                                <Text style={styles.bookPropertiesApi}>{book.category}</Text>
                            </View>
                        </View>
                        <Text style={styles.bookComments}>RESENHA DA EDITORA</Text>
                        <Text style={styles.bookCommentsText}>{book.description}</Text>
                    </View>        
                </View>}
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',  
    },
    content:{ 
        width: '100%',
        marginTop: 42,
    },
    buttonBack:{
        marginTop: 16,
        marginLeft: 16,
        marginBottom: 12,
    },
    bookImage:{
        maxWidth: '100%',
        maxHeight: 380,
        height: 380,
        resizeMode: 'contain',
        marginBottom: 24,
    },
    textContainer:{
        paddingHorizontal: '15%',
        alignSelf: 'center',
    },
    bookTitle:{
        textAlign: 'justify', 
        alignSelf: 'flex-start',
        fontSize: 28,
        color: colors.black_light,
        fontFamily: fonts.medium,
        lineHeight: 40,
    },
    bookAuthor:{
        textAlign: 'center', 
        alignSelf: 'flex-start',
        fontSize: 12,
        color: colors.purple,
        fontFamily: fonts.medium,
        lineHeight: 20,
        marginBottom: 32,
    },
    bookInformation:{
        textAlign: 'center',
        alignSelf: 'flex-start',
        fontSize: 12,
        color: colors.black_light,
        fontFamily: fonts.medium,
        lineHeight: 28,
    },
    bookInformationList:{
        marginBottom: 16,
    },
    bookPropertiesGroup:{
        width: '100%',
        flexDirection: 'row',
    },
    bookProperties:{
        width: '50%',
        textAlign: 'left',
        alignSelf: 'flex-start',
        fontSize: 12,
        color: colors.black_light,
        fontFamily: fonts.medium,
        lineHeight: 20,
    },
    bookPropertiesApi:{
        width: '50%',
        textAlign: 'right',
        alignSelf: 'flex-end',
        fontSize: 12,
        color: colors.gray_light1,
        fontFamily: fonts.medium,
        lineHeight: 20,
    },
    bookComments:{
        textAlign: 'center',
        alignSelf: 'flex-start',
        fontSize: 12,
        color: colors.black_light,
        fontFamily: fonts.medium,
        lineHeight: 28,
        marginBottom: 12,
    },
    bookCommentsText:{
        textAlign: 'left',
        fontSize: 12,
        color: colors.gray_light1,
        fontFamily: fonts.medium,
        lineHeight: 20,
        marginBottom: 12,
    },
})