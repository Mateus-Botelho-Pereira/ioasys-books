import React, { useState, useEffect }  from 'react'
import { StyleSheet, SafeAreaView, Image, View, TouchableOpacity, Text, TextInput, ScrollView, Modal } from 'react-native'

import ModalComponent from '../components/ModalComponent'

import { useNavigation } from '@react-navigation/core'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { RootStackParamList } from '../routes/stack.routes'

import api from '../services/Api'

import LogoBlack from '../../assets/LogoBlack.png'
import LogOut from '../../assets/LogOut.png'
import TestBook from '../../assets/TestBook.png'

import Search from '../../assets/Search.png'
import Filters from '../../assets/Filters.png'

import colors from '../styles/colors'
import fonts from '../styles/fonts'
import { Book } from '../model/book'

export function Home() {

    type homeScreenProp = NativeStackNavigationProp<RootStackParamList, 'Details'>;
    const navigation = useNavigation<homeScreenProp>()

    const [modalVisible, setModalVisible] = useState(false);

    const [searchText, setSearchText] = useState('')

    function bookDetails (id: any){
        navigation.navigate('Details', {id: id})
    }
    
    var [booksReturn, setBooksReturn] = useState<Book[]>([])
    var [booksFilter, setBooksFilter] = useState<Book[]>([])
    var [booksListPages, setBooksListPages] = useState<Book[]>([])

    var [page, setPage] = useState(1)
    var [totalPage, setTotalPage] = useState(1)

    useEffect(() => {
        getBookRequest(page)
    }, []);

    const getBookRequest = async (page: number) => {
        const params = {
            page: page,
            amount: (page * 10),
        }

        const { data: books } = await api.get('/books', {
            params: params
        })

        setBooksReturn(books.data)
        setBooksFilter(books.data)

        setTotalPage(books.totalPages)
    }

    function searchEvent (){
        if(searchText != ''){
            setBooksReturn(booksFilter.filter(item => 
                item.title.toLowerCase().includes(searchText.toLowerCase()) ||
                item.authors.includes(searchText.toLowerCase())
            ))
        }else{
            setBooksReturn(booksFilter)
            alert('Digite algo na barra de pesquisa')
        } 
    }

    function seeMore(){
        if(page < totalPage){
            setPage(page + 1)
            getBookRequest(page + 1)
            setBooksListPages(booksListPages.concat(booksReturn))
            setBooksReturn(booksListPages)
        }
        else{
            alert('Não existem mais livros disponíveis')
        }
    }

    return (
        <SafeAreaView style={styles.container}>

            <ModalComponent active={modalVisible} setActive={setModalVisible}/>

            <View style={styles.content}>
                <View style={styles.header}>
                    <Image style={styles.logo} source={LogoBlack}/>
                    <Text style={styles.title}>Books</Text>
                    <TouchableOpacity style={styles.buttonLogOut} onPress={() => navigation.goBack()}>
                        <Image source={LogOut}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.filters}>
                    <View style={styles.groupInputButton}>
                        <TextInput
                            style={styles.inputText}
                            placeholder='Procure um livro'
                            keyboardType='default'
                            onChangeText={text => setSearchText(text)}
                        />
                        <TouchableOpacity style={styles.buttonSearch} onPress={() => searchEvent()}> 
                            <Image source={Search}/>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={styles.buttonFilter} onPress={() => setModalVisible(true)}> 
                        <Image source={Filters}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.booksList}>
                <ScrollView>
                    {booksReturn.length > 0 && booksReturn.map(({title, pageCount, authors, publisher, published, imageUrl, id}, index) => (
                    <TouchableOpacity key={index} onPress={() => bookDetails(id)}>
                        <View style={styles.bookBox}>
                            <Image style={styles.bookImage} source={{uri: imageUrl}}/>
                            <View style={styles.boxDescription}>
                                <View style={styles.boxDescriptionTitle}>
                                    <Text style={styles.bookTitle}>{title}</Text>
                                    {/*@ts-ignore*/}
                                    {authors.map((author, index) => (
                                        <Text key={index} style={styles.bookAuthor}>{author}</Text>
                                    ))}
                                </View>
                                <View style={styles.boxDescriptionData}>
                                    <Text style={styles.bookData}>{pageCount} páginas</Text>
                                    <Text style={styles.bookData}>Editora {publisher}</Text>
                                    <Text style={styles.bookData}>Publicado em {published}</Text>
                                </View>   
                            </View>     
                        </View>
                    </TouchableOpacity>
                    ))}
                    <TouchableOpacity style={styles.verMaisButton} onPress={() => seeMore()}>
                        <Text style={styles.verMaisText}>VER MAIS</Text>
                    </TouchableOpacity>
                </ScrollView>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white_dark,
    },
    content:{ 
        justifyContent: 'center',
        width: '90%',
        marginTop: 42,
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 32,    
    },
    logo:{
        marginRight: 20,
    },
    title:{
        fontSize: 28,
        color: colors.black_light,
        fontFamily: fonts.light,
    },
    buttonLogOut:{
        position: 'absolute',
        right: 0,
    },
    filters:{
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 32,
    },
    groupInputButton:{
        width: '90%',
        justifyContent: 'center',
    },
    inputText:{
        height: 48,
        width: '100%',
        backgroundColor: colors.white_dark,
        alignItems: 'center',
        fontSize: 12,
        padding: 10,
        color: colors.gray_light1,
        fontFamily: fonts.medium,
        borderRadius: 2,
        borderColor: colors.gray_light2,
        borderStyle: 'solid',
        borderWidth: 1,
    },
    buttonSearch:{
        position: 'absolute',
        right: 18,
    },
    buttonFilter:{
        position: 'absolute',
        right: 0,
    },
    booksList:{
        height: '80%',
        width: '100%',
    },
    bookBox:{
        height: 200,
        width: '100%',
        borderRadius: 8,
        marginBottom: 16,
        padding: 16,
        flexDirection: 'row',
        backgroundColor: colors.white,    
    },
    bookImage:{
        width: '40%',
        height: '100%',
        resizeMode: 'contain',
    },
    boxDescription:{
        width: '60%',
        height: '100%',
    },
    boxDescriptionTitle:{
        width: '100%',
        height: '50%',
    },
    boxDescriptionData:{
        width: '100%',
        height: '50%',
        justifyContent: 'flex-end',
    },
    bookTitle:{
        textAlign: 'left', 
        alignSelf: 'flex-start',
        fontSize: 16,
        color: colors.black_light,
        fontFamily: fonts.medium,
        lineHeight: 26,
    },
    bookAuthor:{
        textAlign: 'left', 
        alignSelf: 'flex-start',
        fontSize: 12,
        color: colors.purple,
        fontFamily: fonts.medium,
        lineHeight: 20,
    },
    bookData:{
        textAlign: 'left',
        alignSelf: 'flex-start',
        fontSize: 12,
        color: colors.gray_light1,
        fontFamily: fonts.text,
        lineHeight: 20,
    },
    verMaisButton: {
        height: 32,
        alignSelf: 'center',
        paddingHorizontal: 16,
        borderRadius: 44,
        marginTop: 16,
        marginBottom: 20,
        borderColor: colors.black_light,
        borderStyle: 'solid',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    verMaisText: {
        fontSize: 12, 
        color: colors.black_light,
        fontFamily: fonts.text,
    },
})