import React, { useState } from 'react'
import { StyleSheet, SafeAreaView, ImageBackground, Image, View, TextInput, TouchableOpacity, Text, Alert} from 'react-native'

import { useNavigation } from '@react-navigation/core'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { RootStackParamList } from '../routes/stack.routes'

import { instance } from '../services/Api'
import AsyncStorage from '@react-native-async-storage/async-storage'

import BackgroundImage from '../../assets/BackgroundImage.png'
import Logo from '../../assets/Logo.png'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

export function Login() {

    const [Email, setEmail] = useState('')
    const [Password, setPassword] = useState('')

    type homeScreenProp = NativeStackNavigationProp<RootStackParamList, 'Home'>;
    const navigation = useNavigation<homeScreenProp>()

    const loginEvent = async () => {
        const body = {
            "email": Email,
            "password": Password
        }
        try {
            const { data: user, headers } = await instance.post('/auth/sign-in', body)

            const token = headers['authorization']
            const refresh_token = headers['refresh-token']

            await AsyncStorage.setItem('@books-app:user-token', String(token))
            navigation.navigate('Home')
        } catch (e: any) {
            alert(JSON.stringify(e.response.data.errors.message))
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <ImageBackground source={BackgroundImage} resizeMode='cover' style={styles.image}>
                <View style={styles.content}>
                    <View style={styles.header}>
                        <Image style={styles.logo} source={Logo}/>
                        <Text style={styles.title}>Books</Text>
                    </View>
                    <View style={styles.inputBox}>
                        <View style={styles.groupInputButton}>
                        <TextInput
                            style={styles.inputText}
                            keyboardType='default'
                            onChangeText={text => setEmail(text)}
                        />
                        <Text style={styles.inputTextLabel}>Email</Text>
                        </View>
                        <View style={styles.groupInputButton}>
                            <TextInput
                                style={styles.inputText}
                                keyboardType='default'
                                secureTextEntry={true}
                                onChangeText={text => setPassword(text)}
                            />
                            <Text style={styles.inputTextLabel}>Senha</Text>
                            <TouchableOpacity style={styles.button} onPress ={loginEvent}> 
                                <Text style={styles.buttonText}>Entrar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    image:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    content:{ 
        justifyContent: 'center',    
        width: '90%',
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 50,
    },
    logo:{
        marginRight: 20,
    },
    title:{
        fontSize: 28,
        color: colors.white,
        fontFamily: fonts.light,
    },
    inputBox:{
        alignItems: 'center',
    },
    inputText:{
        height: 60,
        width: '100%',
        paddingTop: 20,
        paddingLeft: 12,
        backgroundColor: colors.gray,
        borderRadius: 4,
        alignItems: 'center',
        fontSize: 16, 
        color: colors.white,
        fontFamily: fonts.text,
        marginBottom: 16,
    },
    groupInputButton:{
        width: '100%',
        justifyContent: 'center',
    },
    inputTextLabel: {
        position: 'absolute',
        top: 8,
        left: 12,
        color: colors.gray_light1,
        fontSize: 12,
        fontFamily: fonts.text,
    },
    button:{
        position: 'absolute',
        bottom: 28,
        right: 16,
        height: 36,
        width: 85,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        borderRadius: 44,
    },
    buttonText:{
        fontSize: 16, 
        color: colors.pink,
        fontFamily: fonts.medium,
    },
})