export default {
    white: '#FFFFFF',
    white_dark: '#F6F5EF',

    black: '#000000',
    black_light: 'rgba(51, 51, 51, 1)',

    gray: 'rgba(52, 52, 52, 0.8)',
    gray_light1: 'rgba(153, 153, 153, 0.7)',
    gray_light2: 'rgba(51, 51, 51, 0.2)',

    pink: 'rgba(178, 46, 111, 1)',

    purple: 'rgba(171, 38, 128, 1)',

}