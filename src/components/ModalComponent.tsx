import React, { useState }  from 'react'
import { StyleSheet, View, TouchableOpacity, Text, Modal, Image, } from 'react-native'

import Close from '../../assets/Close.png'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

const categories: string[] = ['Biografias', 'Coleções', 'Comportamento', 'Contos', 'Crítica literária',
 'Ficção científica', 'Folklore', 'Genealogia', 'Humor', 'Crianças', 'Jogos', 'Jornais']

const years: string[] = ['2015', '2016', '2017', '2018', '2019', '2020', '2021']

type ModalComponentProps = {
    active: boolean
    setActive: Function
}

const ModalComponent = (props: ModalComponentProps) => {
    const { active, setActive } = props

    var [selectedCategories, setSelectedCategories] = useState([''])
    var [selectedYear, setSelectedYear] = useState([''])

    function addOrRemoveCategory(category: string){
        var list = [...selectedCategories]
        const index = selectedCategories.findIndex(item => item == category)

        if(index >= 0){
            list.splice(index, 1)
            setSelectedCategories(prev => [...prev, category])
        }
        else{
            list.push(category)
        }

        setSelectedCategories(list)
    }

    function addOrRemoveYear(year: string){
        var list = [...selectedYear]
        const index = selectedYear.findIndex(item => item == year)

        if(index >= 0){
            list.splice(index, 1)
            setSelectedYear(prev => [...prev, year])
        }
        else{
            list.push(year)
        }

        setSelectedYear(list)
    }

    function filter(){

        

        setActive(!active)
    }

    return(
        <Modal 
            animationType='fade'
            transparent={true}
            visible={active}
            onRequestClose={() => {
              setActive(!active);
            }}
            >
                <View style={styles.modalViewDarkBachground}>
                    <View style={styles.modalView}>
                        <View style={styles.buttonCloseBox}>
                            <TouchableOpacity 
                            style={styles.buttonClose}
                            onPress={() => setActive(!active)}
                            >
                                <Image source={Close}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.modalViewGroup}>
                            <Text style={styles.modalText}>Selecione uma categoria</Text>
                            <View style={styles.modalViewGroupButtons}>
                                {categories.map((category, index) => (
                                    <TouchableOpacity
                                        key={index}
                                        style={
                                            selectedCategories.includes(category) ? 
                                            styles.modalButtonActive 
                                            : 
                                            styles.modalButton
                                        }
                                        onPress={() => addOrRemoveCategory(category)}
                                    >
                                        <Text 
                                        style={
                                            selectedCategories.includes(category) ? 
                                            styles.modalButtonTextActive
                                            : 
                                            styles.modalButtonText
                                        }
                                        >{category}</Text>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </View>   
                        <View style={styles.modalViewGroup}>
                            <Text style={styles.modalText}>Selecione um ano</Text>
                            <View style={styles.modalViewGroupButtons}>
                                {years.map((year, index) => (
                                    <TouchableOpacity
                                        key={index}
                                        style={
                                            selectedYear.includes(year) ? 
                                            styles.modalButtonActive 
                                            : 
                                            styles.modalButton
                                        }
                                        onPress={() => addOrRemoveYear(year)}
                                    >
                                        <Text 
                                        style={
                                            selectedYear.includes(year) ? 
                                            styles.modalButtonTextActive
                                            : 
                                            styles.modalButtonText
                                        }
                                        >{year}</Text>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </View>
                            <TouchableOpacity
                            style={styles.modalButtonFiltrar}
                            onPress={() => filter()}
                            >
                                <Text style={styles.modalButtonFiltrarText}>Filtrar</Text>
                            </TouchableOpacity>
                    </View>
                </View>
        </Modal>
    )
}

export default ModalComponent

const styles = StyleSheet.create({
    modalViewDarkBachground: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.8)',
    },
    modalView: {
        width: '90%',
        backgroundColor: colors.white,
        borderRadius: 8,
        padding: 16,
        alignItems: 'center',
    },
    buttonCloseBox:{
        width: '100%',
        alignItems: 'flex-end',
    },
    buttonClose:{

    },
    modalViewGroup: {
        width: '100%',
        justifyContent: 'flex-start',
        marginBottom: 38,
    },
    modalViewGroupButtons:{
        width: '100%',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    modalText: {
        fontSize: 12, 
        color: colors.black_light,
        fontFamily: fonts.medium,
        marginBottom: 8,
    },
    modalButton: {
        height: 32,
        alignSelf: 'flex-start',
        paddingHorizontal: 16,
        borderRadius: 44,
        marginRight: 8,
        marginBottom: 8,
        borderColor: colors.black_light,
        borderStyle: 'solid',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalButtonActive: {
        height: 32,
        alignSelf: 'flex-start',
        paddingHorizontal: 16,
        borderRadius: 44,
        marginRight: 8,
        marginBottom: 8,
        backgroundColor: colors.black_light,
        borderStyle: 'solid',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalButtonText: {
        fontSize: 12, 
        color: colors.black_light,
        fontFamily: fonts.text,
    },
    modalButtonTextActive: {
        fontSize: 12, 
        color: colors.white,
        fontFamily: fonts.text,
    },
    modalButtonFiltrar: {
        height: 36,
        width: 96,
        borderRadius: 44,
        borderColor: colors.pink,
        borderStyle: 'solid',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalButtonFiltrarText: {
        fontSize: 16, 
        color: colors.pink,
        fontFamily: fonts.medium,
    },
})