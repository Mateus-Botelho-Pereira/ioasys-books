import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import { Login } from '../pages/Login'
import { Home } from '../pages/Home'
import { Details } from '../pages/Details'

export type RootStackParamList = {
    Login: undefined;
    Home: undefined;
    Details: any;
};
  
const stackRoutes = createNativeStackNavigator<RootStackParamList>();
  
export const Routes = () => {
    
    return (
        <NavigationContainer>       
            <stackRoutes.Navigator 
                screenOptions={{
                headerShown: false
                }}
                initialRouteName='Login'
            >
            <stackRoutes.Screen
                name='Login'
                component={Login}
            />

            <stackRoutes.Screen
                name='Home'
                component={Home}
            />

            <stackRoutes.Screen
                name='Details'
                component={Details}
            />
            </stackRoutes.Navigator>
        </NavigationContainer>
    );
};