Aplicativo construído com React Native e Expo

Node.js v14.17.6
Yarn    v1.22.10

Bibliotecas usadas:

expo fonts                              //Utilizada para instalar a fonte Heebo
expo expo-app-loading                   //Utilizado para não deixar o App abrir antes de carregar as dependências
react-navigation   //Utilizado para fazer a navegação entre as telas
expo react-native-screens react-native-safe-area-context    //Utilizado na navegação
react-navigation/native-stack                               //Utilizado na pilha de navegação
expo axios                                                  //Para fazer requests
expo react-native-async-storage/async-storage               //Armazenar dados no async-storage do celular