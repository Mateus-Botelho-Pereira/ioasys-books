import React from 'react'

import { Routes } from './src/routes/stack.routes'

import AppLoading from 'expo-app-loading'

import {
  useFonts,
  Heebo_300Light,
  Heebo_400Regular,
  Heebo_500Medium,
} from '@expo-google-fonts/heebo'

export default function App(){
  const [ fontsLoaded ] = useFonts({
    Heebo_300Light,
    Heebo_400Regular,
    Heebo_500Medium,
  })

  if(!fontsLoaded)
    return <AppLoading />

  return(
    <Routes />
  )    
}